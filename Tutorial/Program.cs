﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Tutorial
{
  public static class Program
  {
    [STAThread]
    static void Main(string[] args)
    {
      Application.EnableVisualStyles();
      Application.SetCompatibleTextRenderingDefault(false);

      FrmTest f = new FrmTest();
      f.Show();
      f.Focus();
      System.Windows.Forms.Application.Run(f);
      

      //DownloadMetarForHamburgSynchronically();

      //DownloadMetarForHamburgAsynchronically();

      //DecodeAndEncodeMetar();

      //PrintShortInfo();

      //PrintLongInfo();
    }
  }
}
