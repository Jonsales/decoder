﻿using ENG.WMOCodes.Codes;
using System.Collections.Generic;
using System.Web.Mvc;

namespace WebApplication1.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            List<Metar> listMetarDecoder = new List<Metar>();
            string[] metarCode = new string[]
            {
                "METAR SBGR 161300Z VRB02KT 9999 FEW011 18/14 Q1021="
                , "METAR SBGR 131500Z 34011G22KT CAVOK 24/12 Q1017 WS R09R="
                , "METAR SBRJ 231200Z 31015G27KT 4000 -RA SCT020 BKN120 25/20 Q1012="
                , "METAR SBRJ 231200Z 31015G27KT 280V350 4000 -RA SCT020 BKN120 25/20 Q1012="
                , "SPECI SBGR 290745Z 08009KT 0400 R27R/0750N R27L/1600D R09R/1700D R09L/1900D FG OVC002 10/09 Q1022="
                , "METAR COR SBGR 171200Z 03002KT 1200 R09R/0700U R09L/0700U R27R/0700U R27L/0700U BR FEW002 12/12 Q1022="
                , "METAR SBGR 210900Z 08003KT 5000 2000E R27L/0400N R09L/0600D R27R/0600N R09R/P2000 BCFG FEW003 13/13 Q1023="
                , "SPECI SBGR 101135Z 06009KT 6000 1500E R09R/P2000 R09L/P2000 R27R/P2000 R27L/P2000 BCFG BKN004 BKN010 13/13 Q1025="
            };

            foreach (var strMetarCode in metarCode)
            {
                var metarDecoder = DecodingMetar.DecodeAndEncodeMetar(strMetarCode);
                listMetarDecoder.Add(metarDecoder);
            }

            ViewBag.ListMetar = listMetarDecoder;
            ViewBag.Title = "Home Page";

            return View();
        }
    }
}
